package com.ecommerce.message.producer.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <b>Class</b>: RabbitMqEnvConfig <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RabbitMqEnvConfig {
  private String exchange;
  private String queue;
  private String routingKey;
}
