package com.ecommerce.message.producer.config;

import lombok.RequiredArgsConstructor;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <b>Class</b>: RabbitMqConfig <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Configuration
@RequiredArgsConstructor
public class RabbitMqConfig {
  private final RabbitMqEnvConfig rabbitMqEnvConfig;

  @Bean
  public Queue queue() {
    return new Queue(rabbitMqEnvConfig.getQueue(), true, false, false);
  }

  @Bean
  public Exchange exchange() {
    return new DirectExchange(rabbitMqEnvConfig.getExchange());
  }

  @Bean
  public Binding binding(final Queue queue, final DirectExchange exchange) {
    return BindingBuilder.bind(queue).to(exchange).with(rabbitMqEnvConfig.getRoutingKey());
  }

  @Bean
  public MessageConverter jsonMessageConverter() {
    return new Jackson2JsonMessageConverter();
  }

  /**
   * rabbitTemplate .
   *
   * @param connectionFactory .
   * @return
   */
  @Bean
  public AmqpTemplate rabbitCustomTemplate(final ConnectionFactory connectionFactory) {
    final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(jsonMessageConverter());
    return rabbitTemplate;
  }

}
