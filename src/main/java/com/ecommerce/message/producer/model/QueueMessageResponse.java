package com.ecommerce.message.producer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: QueueMessageResponse <br/>
 * .
 *
 * @author Carlos <br/>
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class QueueMessageResponse {
  private String messageId;

}
