package com.ecommerce.message.producer.model;

import com.ecommerce.message.producer.model.dto.OrderDto;
import com.ecommerce.message.producer.model.dto.PaymentDto;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: QueueMessage <br/>
 * .
 *
 * @author Carlos <br/>
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class QueueMessage {
  @Builder.Default
  private String messageId = UUID.randomUUID().toString();
  private OrderDto order;
  private PaymentDto payment;
}
