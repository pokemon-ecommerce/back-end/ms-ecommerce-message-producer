package com.ecommerce.message.producer.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: OrderDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PaymentDto {
  private String cardNumber;
  private String dueDate;
}
