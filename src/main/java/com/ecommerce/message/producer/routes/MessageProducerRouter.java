package com.ecommerce.message.producer.routes;

import com.ecommerce.message.producer.handlers.MessageProducerHandler;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicate;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * <b>Class</b>: MessageProducerRouter <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Configuration
public class MessageProducerRouter {
  private static final RequestPredicate ROUTE_ACCEPT = RequestPredicates.accept(MediaType.APPLICATION_JSON);

  /**
   * sendMessage .
   *
   * @param messageProducerHandler .
   * @return
   */
  @Bean
  public RouterFunction<ServerResponse> sendMessageRoutes(final MessageProducerHandler messageProducerHandler) {
    return RouterFunctions.route()
        .path("/message", builder -> builder.POST("/publish", ROUTE_ACCEPT, messageProducerHandler::sendMessage))
        .build();
  }

}
