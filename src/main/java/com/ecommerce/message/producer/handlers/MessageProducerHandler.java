package com.ecommerce.message.producer.handlers;

import com.ecommerce.message.producer.config.RabbitMqEnvConfig;
import com.ecommerce.message.producer.model.QueueMessage;
import com.ecommerce.message.producer.model.QueueMessageResponse;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: MessageProducerHandler <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Component
@RequiredArgsConstructor
public class MessageProducerHandler {
  private final AmqpTemplate rabbitCustomTemplate;
  private final RabbitMqEnvConfig rabbitMqEnvConfig;

  public static final Mono<ServerResponse> notFound = ServerResponse.notFound().build();

  /**
   * sendMessage .
   *
   * @param request .
   * @return
   */
  public Mono<ServerResponse> sendMessage(final ServerRequest request) {
    return request.bodyToMono(QueueMessage.class)
        .flatMap(message -> {
          rabbitCustomTemplate.convertAndSend(rabbitMqEnvConfig.getExchange(), rabbitMqEnvConfig.getRoutingKey(), message);
          final QueueMessageResponse messageResponse = QueueMessageResponse.builder().messageId(message.getMessageId()).build();
          return ServerResponse.ok()
              .contentType(MediaType.APPLICATION_JSON)
              .body(BodyInserters.fromValue(messageResponse));
        }).switchIfEmpty(notFound);
  }
}
